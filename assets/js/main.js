showSearchBar = () => {
  let search = document.getElementsByClassName('search')
  search[0].classList.toggle("active")
}

validateForm = () => {
  let error = document.getElementsByClassName('form-group')
  const name = document.forms["loginForm"]["username"].value
  const password = document.forms["loginForm"]["password"].value
  if (name == "") {
    error[0].classList.add("error")
    return false
  }
  else if (password == "") {
    error[0].classList.remove("error")
    error[1].classList.add("error")
    return false
  }
  else {
    getUser(name, password)
  }
}

getUser = (name, password) => {
  $.ajax({
    // url: 'http://localhost:4000/',
    type: 'GET',
    headers: {
      "Authorization": "Basic " + btoa(name + ":" + password)
    },
    data: { username: name, password: password },
    success: (response) => {
      console.log(username)
      localStorage.setItem('username', name)
      localStorage.setItem('password', password)
      localStorage.setItem('Authorization', "Basic " + btoa(name + ":" + password))
      window.location.href = "/home.html";
    }
  })
  return false
}


